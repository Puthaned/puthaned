const products = [
  {
    id: 1,
    name: "หมวก",
    image: "../public/product1.png",
    price: 2000,
    quantity: 1,
  },
  {
    id: 2,
    name: "หูฟัง",
    image: "../public/product2.jpg",
    price: 1500,
    quantity: 3,
  },
  {
    id: 3,
    name: "กระเป๋า",
    image: "../public/product3.jpeg",
    price: 1000,
    quantity: 1,
  },
];

export default products;
