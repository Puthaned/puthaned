import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import "./Details.css";
import blogs from "../data/blog";
export default function Details() {
  const { id } = useParams();
  const [title, setTitle] = useState("");
  const [image, setImage] = useState("");
  const [content, setContent] = useState("");
  const [author, setAuthor] = useState("");
  useEffect(() => {
    const result = blogs.find((item) => item.id === parseInt(id));
    setTitle(result.title);
    setImage(result.image_url);
    setContent(result.content);
    setAuthor(result.author);
  }, []);
  return (
    <div className="container">
      <h2>รายละเอียดบทความ : {title}</h2>
      <img src={image} alt={title} className="blog-image"/>
      <div className="blog-detail">
        <strong>
          ผู้เขียน : {author}
          <p>{content}</p>
        </strong>
      </div>
    </div>
  );
}
